+++
title = "some title"
+++

* storage box by Hetzner
* subfolder via gftp
* subuser for backups
* install restic

* change ~/.ssh/config
* restic -r sftp:xps13-backup:/xps13-backup init

* exclude
 * .cache


* set env var for restic pwd
* upload ssh key for access

 restic -r sftp:xps13-backup:/xps13-backup backup ~/  --exclude-file exclude.txt

The index page
