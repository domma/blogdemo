+++
title = "CSS notes"
+++

# Layout modes

## Flow

* Block elements are stacked in block direction (usually top to bottom) and gready in the inline direction (usually left to right).
* Inline elements are stacked in inline direction and don't consume space.
* Padding only works for block or inline-block elements.


## Absolute


## Flex box

* Elements are layouted in the primary axis, the aligend in the cross axis.
* Block elements are not greedy!
* 'flex: ...' changes the layout within the container.
* Important css properties:
  * flex-direction
  * justify-content (primary axis)
  * align-items (cross axis)
  * flex-shrink and flex-grow controll size ratios between elements
* "display: flex" is not inherited to children


## Grid
